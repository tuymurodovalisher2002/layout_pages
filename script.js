const navbar = document.querySelector(".header");
fetch("./navbar.html")
  .then((res) => res.text())
  .then((data) => (navbar.innerHTML = data));

const route = (event) => {
  event = event || window.event;
  event.preventDefault();
  window.history.pushState({}, "", event.target.href);
  handleLocation();
};
const routes = {
  404: "/404.html",
  "/": "/index.html",
  "/second": "/second.html",
  "/thirth": "/thirth.html",
  "/index.html": "/index.html"
};
const handleLocation = async () => {
  const path = window.location.pathname;
  const router = routes[path] || routes[404];
  const html = await fetch(router).then((data) => data.text());
  document.querySelector(".main").innerHTML = html;
};

window.onpopstate = handleLocation;
window.route = route;
handleLocation();
